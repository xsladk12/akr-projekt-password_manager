import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import os.path, random, re, time, pyotp, pyqrcode, hashlib, json, base64, uuid, smtplib, png, sys, os, string
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from tkinter import messagebox
from base64 import b64encode
from pathlib import Path

username = ""
password = ""
filename = ""
salt_file = ""
authKey = ""
email = ""
count = 0

#Najde soubor se salty pro šifrování hesel v složce data
def findSaltFile():
    if not os.path.exists("data"):
        os.makedirs("data")
    global salt_file
    key = b"eJFU12s-ctaG1oVw0D-9gFKojsfUjIBaDWEG8lg57pw="
    f = Fernet(key)
    for file in os.listdir("./data/"):
        with open("./data/" + file, 'rb') as enc_file:
            encrypted = enc_file.read()
        try:
            filedata = f.decrypt(encrypted)
            salt_file = file
            return
        except:
            continue

    if salt_file == "":
        filename = str(uuid.uuid4()) +'.data'
        file = open('./data/'+ filename, 'wb')
        salt_file = filename
        return 

#Třída pro přihlášení
class Login:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.authKey = []
        self.data = []

    #funkce dešifrování souboru
    def decryptFile(self):
        statement = "nenalezen"
        #Vyzkoušení zda username jiz existuje
        for file in os.listdir("./data/"):
            proces = decryptFile(self.username, self.password, file)
            if(proces == False):
                pass
            else:
                statement = "nalezen"
                global filename
                filename = file
                self.data = proces

        if(statement == "nalezen"):
            global username
            global password
            username = self.username
            password = self.password
            return statement
        else:
            return statement

    #funkce ověření 2FA
    def validate2FA(self, code):
        global filename
        data = decryptFile(self.username, self.password, filename)
        authKey = data[0]['authKey']
        

        totp = pyotp.TOTP(authKey)

        if totp.verify(code) == False:
            return "neovereno"
        else:
            return "overeno"


#třída pro registraci
class Register:

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email
        self.authKey = []
        self.data = []
        self.filename = ""
        self.UsernameSalt = ''
        self.PasswordSalt = ''

    #fuknce kontroly správnosti vyplněných údajů 
    def verifyCredentials(self):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if  (self.username != "" and self.password != "" and self.email != "") and(re.fullmatch(regex, self.email)):
            global salt_file
            with open("./data/" + salt_file, 'rb') as enc_file:
                encrypted = enc_file.read()
            key = b"eJFU12s-ctaG1oVw0D-9gFKojsfUjIBaDWEG8lg57pw="
            f = Fernet(key)
            filedata = f.decrypt(encrypted)
            filedata = filedata.decode('utf8')
            filedata = json.loads(filedata)
            #Vyzkoušení zda username jiz existuje
            statement = "nezabrane"
            for file in os.listdir("./data/"):
                
                for i in range(len(filedata)):
                    salt = ''
                    if (filedata[i]["filename"] == file):
                        salt = filedata[i]["UsernameSalt"].encode('ascii')
                        
                    with open("./data/" + file, 'rb') as enc_file:
                        encrypted = enc_file.read()
                    try:
                        password2 = self.username.encode('ascii')
                        kdf = PBKDF2HMAC(
                            algorithm=hashes.SHA256(),
                            length=32,
                            salt= salt,
                            iterations=100000,
                        )
                        key = base64.urlsafe_b64encode(kdf.derive(password2))
                        
                        f = Fernet(key)
                        token = f.decrypt(encrypted)
                        statement = "zabrane"
                    except:
                        pass

        
            if(statement == "nezabrane"):
                global username
                global password
                global email

                username = self.username
                password = self.password
                email = self.email
                self.authKey = pyotp.random_base32()
                global authKey
                authKey = self.authKey
                link = pyotp.totp.TOTP(self.authKey).provisioning_uri(name='Password manager', issuer_name=self.username)
                url = pyqrcode.create(link)
                url.png('QRcode.png', scale=5.5)
                
                return "nezabrane"
            else:
                return "zabrane"
                 
        else:
            return "spatnevyplnene"
            

    #funkce generování 2FA QR kódu
    def generate2FA(self, code):
        global authKey
        totp = pyotp.TOTP(authKey)
        if totp.verify(code) == False:
            return
        else:
            self.makeFile()
            return "overeno"


        self.makeFile()

    #funkce pro vytvoření saltu pro následující šifrování hesla a uložení jej do souboru
    def generateSalt(self):
        global salt_file
        self.filename = str(uuid.uuid4())+'.data'
        self.UsernameSalt = b64encode(os.urandom(16))
        self.PasswordSalt = b64encode(os.urandom(16))
        global filename
        filename = self.filename
        filedata = []
        key = b"eJFU12s-ctaG1oVw0D-9gFKojsfUjIBaDWEG8lg57pw="
        f = Fernet(key)
        block = {
            'filename': self.filename,
            'UsernameSalt': self.UsernameSalt.decode('utf-8'),
            'PasswordSalt': self.PasswordSalt.decode('utf-8'),
        }
        with open("./data/" + salt_file, 'rb') as enc_file:
            encrypted = enc_file.read()
        try:
            filedata = f.decrypt(encrypted)
            filedata = filedata.decode('ascii')
            filedata = json.loads(filedata)
        except:
            pass
        filedata.append(block)
        filedata = json.dumps(filedata)
        encrypted_data = f.encrypt(filedata.encode("ascii"))

        with open('./data/'+ salt_file, 'wb') as outfile:
            outfile.write(encrypted_data)


    #Funkce pro vytvoření složky a vložení json dat do souboru
    def makeFile(self):
        global authKey
        self.authKey = authKey

        block = {
            'username': self.username,
            'password': self.password,
            'email': self.email,
            'authKey': self.authKey,
        }
        self.data.append(block)

        data = json.dumps(self.data).encode('utf-8')
        global filename
        
        encryptFile(self.username, self.password, filename, data)

        return block

#Funkce pro dešifrování dat
def decryptFile(username, password, filename):
    global salt_file
    with open("./data/" + salt_file, 'rb') as enc_file:
        encrypted = enc_file.read()
    key = b"eJFU12s-ctaG1oVw0D-9gFKojsfUjIBaDWEG8lg57pw="
    f = Fernet(key)
    filedata = f.decrypt(encrypted)
    filedata = filedata.decode('utf8')
    filedata = json.loads(filedata)
    for i in range(len(filedata)):
        salt = ''
        if (filedata[i]["filename"] == filename):
            UsernameSalt = filedata[i]["UsernameSalt"].encode('ascii')
            PasswordSalt = filedata[i]["PasswordSalt"].encode('ascii')
            
    with open("./data/" + filename, 'rb') as enc_file:
        encrypted = enc_file.read()
    
    try:
        password1 = username.encode('ascii')
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt= UsernameSalt,
            iterations=100000,
        )
        key = base64.urlsafe_b64encode(kdf.derive(password1))
        f = Fernet(key)
        token = f.decrypt(encrypted)
        password2 = password.encode('ascii')
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt= PasswordSalt,
            iterations=100000,
        )
        key = base64.urlsafe_b64encode(kdf.derive(password2))
        f = Fernet(key)
        data = f.decrypt(token)
        data = data.decode('utf8')
        data = json.loads(data)
        return data
    except:
        return False

#Funkce pro šifrování dat
def encryptFile(username, password, filename, data):
    global salt_file
    with open("./data/" + salt_file, 'rb') as enc_file:
        encrypted = enc_file.read()
    key = b"eJFU12s-ctaG1oVw0D-9gFKojsfUjIBaDWEG8lg57pw="
    f = Fernet(key)
    filedata = f.decrypt(encrypted)
    filedata = filedata.decode('utf8')
    filedata = json.loads(filedata)
    for i in range(len(filedata)):
        salt = ''
        if (filedata[i]["filename"] == filename):
            UsernameSalt = filedata[i]["UsernameSalt"].encode('ascii')
            PasswordSalt = filedata[i]["PasswordSalt"].encode('ascii')
            
    password1 = password.encode('utf-8')
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt= PasswordSalt,
        iterations=100000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(password1))
    f = Fernet(key)
    token = f.encrypt(data)
    password2 = username.encode('utf-8')
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt= UsernameSalt,
        iterations=100000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(password2))
    f = Fernet(key)
    data = f.encrypt(token)
    
    with open('./data/'+ filename, 'wb') as outfile:
        outfile.write(data)

    return data


##################################################################### GUI #####################################################################
#Hlavní třída pro funkci GUI
class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartFrame, LoginFrame, RegisterFrame, TwoFAFrame, MainFrame, UserSettingsFrame , AddRecordFrame, LoginVerifyFrame):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartFrame")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()

    def showQRcode(self):
        frame = self.frames["TwoFAFrame"]
        frame.showQRcode()

    def showData(self):
        frame = self.frames["MainFrame"]
        frame.showData()

    def addNewRecord(self,record_username,record_password,website):
        frame = self.frames["MainFrame"]
        frame.addNewRecord(record_username,record_password,website)

    def showUserSettings(self):
        frame = self.frames["UserSettingsFrame"]
        frame.showUserData()

        

#Třída přihlašovacího okna
class LoginFrame(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        controller.geometry("900x500")
        controller.configure(bg = "#222831")
        canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        canvas.place(x = 0, y = 0)
        canvas.create_rectangle(438.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")

        canvas.create_text(493.0, 217.0, anchor="nw", text="Password", fill="#222831", font=("Roboto Bold", 24 * -1) )
        canvas.create_text(493.0, 82.0, anchor="nw", text="Username", fill="#222831", font=("Roboto Bold", 24 * -1) )
        hlaska = canvas.create_text(560.0, 20.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 24 * -1) )


        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_1.png")
        entry_bg_1 = canvas.create_image(665.5, 164.5, image=self.entry_image_1 ) 
        entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_1.place(x=505.0, y=134.0, width=321.0, height=59.0 )

        self.button_image_3 = tk.PhotoImage(file="./assets/button_3.png")
        button_3 = tk.Button(self, image=self.button_image_3, borderwidth=0, highlightthickness=0, command=lambda: runLogin(), relief="flat") 
        button_3.place(x=576.0, y=379.0, width=180.0, height=55.0 )

        self.entry_image_2 = tk.PhotoImage(file="./assets/entry_2.png")
        entry_bg_2 = canvas.create_image(665.5, 297.5, image=self.entry_image_2 ) 

        entry_2 = tk.Entry(self, bd=0, bg="#E4E4E5",show="*", highlightthickness=0 ) 
        entry_2.place(x=505.0, y=267.0, width=321.0, height=59.0 )

        self.button_image_4 = tk.PhotoImage(file="./assets/button_4.png")
        button_4 = tk.Button(self, image=self.button_image_4, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("StartFrame"), relief="flat") 
        button_4.place(x=22.0, y=18.0, width=42.0, height=34.0 )

        self.image_image_1 = tk.PhotoImage(file="./assets/image_1.png")
        image_1 = canvas.create_image(206.0, 267.0, image=self.image_image_1 ) 
        controller.resizable(False, False)

        def runLogin():
            username = entry_1.get()
            password = entry_2.get()
            if Login(username, password).decryptFile() == "nalezen" :
                self.controller.show_frame("LoginVerifyFrame")
                entry_1.delete(0, 'end')
                entry_2.delete(0, 'end')
                canvas.itemconfig(hlaska, text="")

            else:
             canvas.itemconfig(hlaska, text="Incorrect input!")


#Třída počátečního okna
class StartFrame(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        controller.geometry("900x500")
        controller.title("Password Manager")

        canvas = tk.Canvas(self, bg = "#222831",height = 500,width = 900,bd = 0,highlightthickness = 0,relief = "ridge")
        canvas.place(x = 0, y = 0)
        canvas.create_text(159.0,103.0,anchor="nw",text="welcome to ...\n",fill="#FCFCFC",font=("Roboto Bold", 18 * -1))
        canvas.create_text(190.0,143.0,anchor="nw",text="PASSWORD MANAGER",fill="#FCFCFC",font=("Roboto Bold", 48 * -1))
        
        canvas.create_rectangle(60.0,34.0,65.0,307.0,fill="#FCFCFC",outline="")
        canvas.create_rectangle(840.0,193.0,845.0,466.0,fill="#FCFCFC",outline="")
        
        self.image = tk.PhotoImage(file='./assets/button_1.png')
        button_1 = tk.Button(self,image=self.image, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("RegisterFrame"), relief = tk.FLAT)
        button_1.place(x=259.0,y=252.0,width=180.0,height=55.0)

        self.button_image_2 = tk.PhotoImage(file="./assets/button_2.png")
        button_2 = tk.Button(self,image=self.button_image_2,borderwidth=0,highlightthickness=0,command=lambda: controller.show_frame("LoginFrame"),relief="flat")
        button_2.place(x=474.0,y=252.0,width=180.0,height=55.0)
        controller.resizable(False, False)
        
#Třída okna pro registraci
class RegisterFrame(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        controller.configure(bg = "#222831")
        canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        canvas.place(x = 0, y = 0)
        canvas.create_rectangle(438.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")
        canvas.create_text(486.0, 311.0, anchor="nw", text="E-mail", fill="#222831", font=("Roboto Bold", 24 * -1) )
        hlaska = canvas.create_text(460.0, 435.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 24 * -1) )

        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_3.png")
        entry_bg_1 = canvas.create_image(607.5, 277.0, image=self.entry_image_1 ) 
        entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5",show="*", highlightthickness=0 ) 
        entry_1.place(x=497.0, y=258.0, width=221.0, height=36.0 )

        self.entry_image_2 = tk.PhotoImage(file="./assets/entry_3.png")
        entry_bg_2 = canvas.create_image(607.5, 373.0, image=self.entry_image_2 ) 
        entry_2 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_2.place(x=497.0, y=354.0, width=221.0, height=36.0 )

        self.entry_image_3 = tk.PhotoImage(file="./assets/entry_3.png")
        entry_bg_3 = canvas.create_image(607.5, 181.0, image=self.entry_image_3 ) 
        entry_3 = tk.Entry(self, bd=0, bg="#E5E5E5",show="*", highlightthickness=0 ) 
        entry_3.place(x=497.0, y=162.0, width=221.0, height=36.0 )
        
        canvas.create_text(485.0, 215.0, anchor="nw", text="Retype Password", fill="#222831", font=("Roboto Bold", 24 * -1) )
        canvas.create_text(485.0, 119.0, anchor="nw", text="Password", fill="#222831", font=("Roboto Bold", 24 * -1) )
        canvas.create_text(485.0, 24.0, anchor="nw", text="Username", fill="#222831", font=("Roboto Bold", 24 * -1))
        hlaska = canvas.create_text(470.0, 437.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 17 * -1))

        self.button_image_5 = tk.PhotoImage(file="./assets/button_5.png")
        button_5 = tk.Button(self, image=self.button_image_5, borderwidth=0, highlightthickness=0, command=lambda: runRegister(), relief="flat") 
        button_5.place(x=708.0, y=423.0, width=180.0, height=55.0 )
        
        self.entry_image_4 = tk.PhotoImage(file="./assets/entry_3.png")
        entry_bg_4 = canvas.create_image(607.5, 85.0, image=self.entry_image_4 ) 
        entry_4 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_4.place(x=497.0, y=66.0, width=221.0, height=36.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("StartFrame"), relief="flat") 
        button_2.place(x=22.0, y=18.0, width=42.0, height=34.0 )
        
        self.image_image_1 = tk.PhotoImage(file="./assets/image_1.png")
        image_1 = canvas.create_image(206.0, 267.0, image=self.image_image_1 ) 
        controller.resizable(False, False)

        def runRegister():
            username = entry_4.get()
            password = entry_3.get()
            re_password = entry_1.get()
            email = entry_2.get()

            if ((password == re_password) & (password != "")):
                Register(username, password, email).generateSalt()
                prikaz  = Register(username, password, email).verifyCredentials()
                if (prikaz == "nezabrane"):
                    controller.showQRcode()
                    controller.show_frame("TwoFAFrame")
                    entry_4.delete(0, 'end')
                    entry_3.delete(0, 'end')
                    entry_2.delete(0, 'end')
                    entry_1.delete(0, 'end')
                    canvas.itemconfig(hlaska, text= "")
                elif(prikaz == "spatnevyplnene"):
                    canvas.itemconfig(hlaska, text= "Incorrect input!")
                elif(prikaz == "zabrane"):
                    canvas.itemconfig(hlaska, text= "Username is taken!")
            else:
                canvas.itemconfig(hlaska, text= "Passwords don't match!")



#Třída okna pro prvotní vytvoření a ověření 2FA
class TwoFAFrame(tk.Frame):
    
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller        
        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.canvas.create_rectangle(438.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")
        self.canvas.create_text(481.0, 395.0, anchor="nw", text="Entry 2FA code", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(498.0, 30.0, anchor="nw", text="Scan with Google Authenticator", fill="#222831", font=("Roboto Bold", 24 * -1) )
        hlaska = self.canvas.create_text(730.0, 380.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 24 * -1) )
        
        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_4.png")
        entry_bg_1 = self.canvas.create_image(565.0, 451.0, image=self.entry_image_1 ) 
        entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_1.place(x=493.0, y=432.0, width=144.0, height=36.0 )
        
        self.button_image_6 = tk.PhotoImage(file="./assets/button_6.png")
        button_6 = tk.Button(self, image=self.button_image_6, borderwidth=0, highlightthickness=0, command=lambda: runVerify(), relief="flat") 
        button_6.place(x=708.0, y=423.0, width=180.0, height=55.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: HideQRcode(), relief="flat") 
        button_2.place(x=22.0, y=18.0, width=42.0, height=34.0 )
        
        self.image_image_1 = tk.PhotoImage(file="./assets/image_1.png")
        image_1 = self.canvas.create_image(206.0, 267.0, image=self.image_image_1 )
        
        
        controller.resizable(False, False)

        def runVerify():
            global username
            global password
            global email

            code = entry_1.get()
            if (Register(username, password, email).generate2FA(code) == "overeno"):
                controller.show_frame("LoginFrame")
                self.image_image_2 = tk.PhotoImage(file="")
                image_2 = self.canvas.create_image(672.0, 221.0, image=self.image_image_2 )
                os.remove("./QRcode.png")
                entry_1.delete(0, 'end')
                self.canvas.itemconfig(hlaska, text = "")
            else:
                self.canvas.itemconfig(hlaska, text = "Wrong code!")
                entry_1.delete(0, 'end')

        def HideQRcode():
            controller.show_frame("RegisterFrame")
            self.image_image_2 = tk.PhotoImage(file="")
            image_2 = self.canvas.create_image(672.0, 221.0, image=self.image_image_2 )
            entry_1.delete(0, 'end')

    def showQRcode(self):
        self.image_image_2 = tk.PhotoImage(file="./QRcode.png")
        self.image_2 = self.canvas.create_image(672.0, 221.0, image=self.image_image_2 )


#Třída hlavního okna se záznamy    
class MainFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        canvas.place(x = 0, y = 0)
        canvas.create_rectangle(777.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")

        self.button_image_1 = tk.PhotoImage(file="./assets/button_7.png")
        button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: self.showUserSettings(), relief="flat") 
        button_1.place(x=786.0, y=392.0, width=105.0, height=44.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_8.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("AddRecordFrame"), relief="flat") 
        button_2.place(x=786.0, y=18.0, width=105.0, height=44.0 )
        
        self.button_image_4 = tk.PhotoImage(file="./assets/button_10.png")
        button_4 = tk.Button(self, image=self.button_image_4, borderwidth=0, highlightthickness=0, command=lambda: self.logOut(), relief="flat") 
        button_4.place(x=787.0, y=445.0, width=105.0, height=44.0 )
        
        self.button_image_5 = tk.PhotoImage(file="./assets/button_11.png")
        button_5 = tk.Button(self, image=self.button_image_5, borderwidth=0, highlightthickness=0, command=lambda: self.deleteRecord(), relief="flat") 
        button_5.place(x=786.0, y=75.0, width=105.0, height=44.0 ) 
        self.style = ttk.Style()
        self.style.configure("Treeview", 
        background="#D3D3D3",
        foreground="black",
        rowheight=25,
        fieldbackground="#D3D3D3"
        )
        # Change selected color
        self.style.map('Treeview', 
            background=[('selected', "#7a7a7a")])

        # Treeview Scrollbar
        self.tree_scroll = tk.Scrollbar(self)
        self.tree_scroll.pack()
        self.tree_scroll.place(x=740.0, y=37.0, width=20.0, height=420.0 )


        # Create Treeview
        self.my_tree = ttk.Treeview(self, yscrollcommand=self.tree_scroll.set, selectmode="extended")
        # Pack to the screen
        
        self.my_tree.pack()
        self.my_tree.place(x=37.0, y=37.0, width=700.0, height=420.0 )

        #Configure the scrollbar
        self.tree_scroll.config(command=self.my_tree.yview)

        # Define Our Columns
        self.my_tree['columns'] = ("Username", "Password", "Website")

        # Formate Our Columns
        self.my_tree.column("#0", width=0, stretch=tk.NO)
        self.my_tree.column("Username", anchor=tk.CENTER, width=200)
        self.my_tree.column("Password", anchor=tk.CENTER, width=200)
        self.my_tree.column("Website", anchor=tk.CENTER, width=200)

        # Create Headings 
        self.my_tree.heading("#0", text="", anchor=tk.W)
        self.my_tree.heading("Username", text="Username", anchor=tk.CENTER)
        self.my_tree.heading("Password", text="Password", anchor=tk.CENTER)
        self.my_tree.heading("Website", text="Website", anchor=tk.CENTER)

    def logOut(self):
        
        self.controller.show_frame("StartFrame")      
        
    def showUserSettings(self):
        self.controller.showUserSettings()
        self.controller.show_frame("UserSettingsFrame")

    def deleteRecord(self):
        x = self.my_tree.selection()[0]
        answer = messagebox.askquestion("Delete Password", "Are you sure you want to delete record?")
        if answer == "yes":
            global username
            global password
            global filename
            data = decryptFile(username, password, filename)
            data.pop(int(x)+1)
            data = json.dumps(data).encode('utf-8')
            encryptFile(username, password, filename, data)
            
            self.showData()


    def showData(self):
        global username
        global password
        global filename
        self.my_tree.tag_configure('oddrow', background="white")
        self.my_tree.tag_configure('evenrow', background="lightblue")
        global count

        for record in self.my_tree.get_children():
            self.my_tree.delete(record)
        count=0
        data = decryptFile(username, password, filename)
        for  i  in range(len(data)-1):
            i = i+1
            if count % 2 == 0:
                self.my_tree.insert(parent='', index='end', iid=count, text="", values=(data[i]["username"], data[i]["password"], data[i]["website"]), tags=('evenrow',))
            else:
                self.my_tree.insert(parent='', index='end', iid=count, text="", values=(data[i]["username"], data[i]["password"], data[i]["website"]), tags=('oddrow',))

            count += 1

    def addNewRecord(self,record_username,record_password,website):
        self.my_tree.tag_configure('oddrow', background="white")
        self.my_tree.tag_configure('evenrow', background="lightblue")

        global count
        if count % 2 == 0:
            self.my_tree.insert(parent='', index='end', iid=count, text="", values=(record_username, record_password, website), tags=('evenrow',))
        else:
            self.my_tree.insert(parent='', index='end', iid=count, text="", values=(record_username, record_password, website), tags=('oddrow',))

        count += 1



#Třída okna pro uživatelské nastavení
class UserSettingsFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge") 
        self.canvas.place(x = 0, y = 0)
        
        self.image_image_1 = tk.PhotoImage(file="./assets/image_5.png")
        image_1 = self.canvas.create_image(450.0, 250.0, image=self.image_image_1 )
        self.canvas.create_text(110.0, 320.0, anchor="nw", text="Retype Password :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(110.0, 274.0, anchor="nw", text="New Password : ", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(189.0, 395.0, anchor="nw", text="Enter E-mail Code :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        
        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_5.png")
        self.entry_bg_1 = self.canvas.create_image(458.5, 334.0, image=self.entry_image_1 ) 
        self.entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5",show = "*", highlightthickness=0 ) 
        self.entry_1.place(x=348.0, y=315.0, width=221.0, height=36.0 )
        
        self.entry_image_2 = tk.PhotoImage(file="./assets/entry_6.png")
        self.entry_bg_2 = self.canvas.create_image(497.0, 406.0, image=self.entry_image_2 ) 
        self.entry_2 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        self.entry_2.place(x=425.0, y=387.0, width=144.0, height=36.0 )
        
        self.entry_image_3 = tk.PhotoImage(file="./assets/entry_7.png")
        self.entry_bg_3 = self.canvas.create_image(458.5, 288.0, image=self.entry_image_3 ) 
        self.entry_3 = tk.Entry(self, bd=0, bg="#E5E5E5",show = "*", highlightthickness=0 ) 
        self.entry_3.place(x=348.0, y=269.0, width=221.0, height=36.0 )
        self.pole3 = self.canvas.create_text(138.0, 152.0, anchor="nw", text="E-mail :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(109.0, 214.0, anchor="nw", text="Change Password", fill="#222831", font=("Roboto Bold", 36 * -1) )
        self.pole2 = self.canvas.create_text(138.0, 108.0, anchor="nw", text="Number of Records :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.pole1 = self.canvas.create_text(138.0, 66.0, anchor="nw", text="Username :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.hlaska1 = self.canvas.create_text(601.0, 235.0, anchor="nw", text="", fill="#008000", font=("Roboto Bold", 18 * -1) )
        self.hlaska2 = self.canvas.create_text(634.0, 354.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 18 * -1) )
        
        self.button_image_1 = tk.PhotoImage(file="./assets/button_12.png")
        button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: self.verifyEmailCode(), relief="flat") 
        button_1.place(x=611.0, y=378.0, width=180.0, height=55.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: self.showMainFrame(), relief="flat") 
        button_2.place(x=16.0, y=20.0, width=42.0, height=33.0 )
        
        self.button_image_3 = tk.PhotoImage(file="./assets/button_13.png")
        button_3 = tk.Button(self, image=self.button_image_3, borderwidth=0, highlightthickness=0, command=lambda: self.sendEmail(), relief="flat") 
        button_3.place(x=611.0, y=258.0, width=180.0, height=55.0 )

    def showMainFrame(self):
        self.controller.showData()
        self.controller.show_frame("MainFrame")
        self.canvas.itemconfig(self.hlaska1, text = "")
        self.canvas.itemconfig(self.hlaska2, text = "")
        self.canvas.itemconfig(self.pole1, text = "")
        self.canvas.itemconfig(self.pole2, text = "")
        self.canvas.itemconfig(self.pole3, text = "")

    def showUserData(self):
        global username
        global password
        global filename

        self.data = decryptFile(username, password, filename)

        self.pole3 = self.canvas.create_text(138.0, 152.0, anchor="nw", text="E-mail : " + self.data[0]["email"],fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.pole2 = self.canvas.create_text(138.0, 108.0, anchor="nw", text="Number of Records : "+ str(len(self.data)-1), fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.pole1 = self.canvas.create_text(138.0, 66.0, anchor="nw", text="Username : " +  self.data[0]["username"], fill="#222831", font=("Roboto Bold", 24 * -1) )

    def sendEmail(self):
        global username
        global filename

        self.code = str(random.randint(1000, 99999))

        self.newPassword = self.entry_1.get()
        self.newPasswordAgain = self.entry_3.get()

        if ((self.newPassword == self.newPasswordAgain) & (self.newPassword != self.data[0]['password'])):
            sender = "managerp377@gmail.com"
            receiver = str(self.data[0]['email'])
            password = "PasswordManager.1"
            subject = "Code"
            body = "Your code for authentication is: " + str(self.code)
            message = f"""From: PasswordManager {sender}
            To: {receiver}
            Subject: Password Manager \n
            {body}
            """

            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.starttls()

            try:
                server.login(sender,password)
                server.sendmail(sender, receiver, message)
                self.canvas.itemconfig(self.hlaska1, text = "Email has been sent!", fill = "#008000")

            except smtplib.SMTPAuthenticationError:
                pass
        else:
            self.canvas.itemconfig(self.hlaska1, text = "Passwords don't match!" , fill = "#FF0000")

    def verifyEmailCode(self):
        global username
        global password
        global filename
        password = self.newPassword
        inputcode = self.entry_2.get() 
        if(inputcode == self.code):
            self.data[0]['password'] = self.newPassword
            self.data = json.dumps(self.data).encode('utf-8')
            encryptFile(username, self.newPassword, filename, self.data)
            self.entry_2.delete(0, 'end')
            self.entry_1.delete(0, 'end')
            self.entry_3.delete(0, 'end')
            self.canvas.itemconfig(self.hlaska2, text = "Password changed!", fill = "#008000")
        else:
           self.canvas.itemconfig(self.hlaska2, text = "Wrong code!", fill = "#FF0000")





#třída okna pro přidání záznamů
class AddRecordFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        self.controller = controller
        canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        canvas.place(x = 0, y = 0)
        
        self.image_image_1 = tk.PhotoImage(file="./assets/image_5.png") 
        image_1 = canvas.create_image(450.0, 250.0, image=self.image_image_1 )
        
        self.button_image_1 = tk.PhotoImage(file="./assets/button_14.png")
        button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: addRecord(), relief="flat") 
        button_1.place(x=605.0, y=376.0, width=180.0, height=55.0 )
        canvas.create_text(162.0, 202.0, anchor="nw", text="Password :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        canvas.create_text(162.0, 132.0, anchor="nw", text="Username :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        canvas.create_text(162.0, 272.0, anchor="nw", text="Website :", fill="#222831", font=("Roboto Bold", 24 * -1) )
        
        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_8.png")
        entry_bg_1 = canvas.create_image(458.5, 218.0, image=self.entry_image_1 ) 
        entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_1.place(x=348.0, y=199.0, width=221.0, height=36.0 )
        
        self.entry_image_2 = tk.PhotoImage(file="./assets/entry_8.png")
        entry_bg_2 = canvas.create_image(458.5, 286.0, image=self.entry_image_2 ) 
        entry_2 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_2.place(x=348.0, y=267.0, width=221.0, height=36.0 )
        
        self.entry_image_3 = tk.PhotoImage(file="./assets/entry_8.png")
        entry_bg_3 = canvas.create_image(458.5, 146.0, image=self.entry_image_3 ) 
        entry_3 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_3.place(x=348.0, y=127.0, width=221.0, height=36.0 )
        
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("MainFrame"), relief="flat") 
        button_2.place(x=16.0, y=20.0, width=42.0, height=33.0 )

        self.button_image_3 = tk.PhotoImage(file="./assets/Button_generate.png")
        button_2 = tk.Button(self, image=self.button_image_3, borderwidth=0, highlightthickness=0, command=lambda: generatePassword(), relief="flat") 
        button_2.place(x=617.0, y=199.0, width=109.0, height=38.0 )

        def generatePassword():
            entry_1.delete(0, 'end')
            length = 11
            chars = string.ascii_letters + string.digits + '!@#$%^&*()'
            random.seed = (os.urandom(1024))
            heslo = ''.join(random.choice(chars) for i in range(length))
            entry_1.insert('end' , str(heslo))


        def addRecord():
            global username
            global password
            global filename
            data = decryptFile(username, password, filename)

            website = entry_2.get()
            record_username = entry_3.get()
            record_password = entry_1.get()

            block = {
                'website': website,
                'username': record_username,
                'password': record_password,
            }
            data.append(block)
            data = json.dumps(data).encode('utf-8')
            encryptFile(username, password, filename, data)
            controller.addNewRecord(record_username,record_password,website)
            controller.show_frame("MainFrame")
            entry_3.delete(0, 'end')
            entry_2.delete(0, 'end')
            entry_1.delete(0, 'end')


#Třída okna pro ověření 2FA při přihlašování
class LoginVerifyFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        canvas.place(x = 0, y = 0)
        canvas.create_rectangle(438.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")
        canvas.create_text(586.0, 180.0, anchor="nw", text="Entry 2FA Code", fill="#222831", font=("Roboto Bold", 24 * -1) )
        hlaska = canvas.create_text(600.0, 20.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 24 * -1) )
        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_2.png")
        entry_bg_1 = canvas.create_image(668.5, 254.0, image=self.entry_image_1 ) 
        entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0 ) 
        entry_1.place(x=558.0, y=235.0, width=221.0, height=36.0 )
        self.button_image_1 = tk.PhotoImage(file="./assets/button_6.png")
        button_1 = tk.Button(self, image=self.button_image_1, borderwidth=0, highlightthickness=0, command=lambda: runValidate2FA(), relief="flat") 
        button_1.place(x=708.0, y=408.0, width=180.0, height=55.0 )
        self.button_image_2 = tk.PhotoImage(file="./assets/button_4.png")
        button_2 = tk.Button(self, image=self.button_image_2, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("LoginFrame"), relief="flat") 
        button_2.place(x=22.0, y=18.0, width=42.0, height=34.0 )
        self.image_image_1 = tk.PhotoImage(file="./assets/image_1.png")
        image_1 = canvas.create_image(206.0, 267.0, image=self.image_image_1 )

        def runValidate2FA():
            code = entry_1.get()
            global username
            global password
            if Login(username, password).validate2FA(code) == "overeno":
                controller.showData()
                controller.show_frame("MainFrame")
                entry_1.delete(0, 'end')
                canvas.itemconfig(hlaska, text = "")
            else:
                canvas.itemconfig(hlaska, text = "Wrong code!")
                entry_1.delete(0, 'end')

if __name__ == "__main__":
    findSaltFile()
    app = SampleApp()
    app.mainloop()
